# -*- coding: utf-8 -*-
from sqlalchemy import create_engine, MetaData, Table, Column, Integer, DateTime
from geoalchemy2 import Geometry


engine = create_engine('postgresql://postgres:GeoPandas@localhost/gmgi300')
connection = engine.connect()
metadata = MetaData(bind=engine)
sistprepet = Table('sistprepet', metadata,
                   Column('geometri', Geometry('LineString', 4326), nullable= False),
                   Column('id', Integer, nullable=False, primary_key=True),
                   Column('sist prepet', DateTime),
                   Column('dager_siden', Integer))

sistprepet.drop(checkfirst=True)
sistprepet.create()

connection.execute("""
INSERT INTO sistprepet
SELECT DISTINCT ON (d.edge_id) d.geom, d.edge_id, d.tid as "sist prepet", extract(day from '2011-03-04 00:00:00' - d.tid) as "dager_siden"
FROM(
    SELECT DISTINCT ON (p.tid) e.geom, e.edge_id, p.tid
    FROM prepedata AS p
        LEFT JOIN løyper_topo.edge AS e ON ST_DWithin(p.geometri, e.geom, 15)
    ORDER BY p.tid DESC, ST_Distance_Spheroid(p.geometri, e.geom, 'SPHEROID["WGS 84",6378137,298.257223563]')
    ) AS D
WHERE d.edge_id is not NULL
ORDER BY d.edge_id, d.tid DESC 
""")
