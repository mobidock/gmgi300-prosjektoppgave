# -*- coding: utf-8 -*-
from sqlalchemy import create_engine, MetaData, Table, Column, DateTime
from geoalchemy2 import Geometry, shape
from shapely.geometry import Point

engine = create_engine('postgresql://postgres:GeoPandas@localhost/gmgi300')
connection = engine.connect()
metadata = MetaData(bind=engine)
prepedata = Table('prepedata', metadata,
                  Column('geometri', Geometry('Point', 4326), nullable=False),
                  Column('tid', DateTime, nullable=False, primary_key=True))

prepedata.drop(checkfirst=True)
prepedata.create()

with open('Data/preppemaskin_aas_2010_01-03.txt') as gpsfil:
    data = []
    for line in gpsfil:
        kolonner = line.split(';')
        punkt = Point([float(kolonner[1]), float(kolonner[0])])
        tid = kolonner[2]
        data.append({'geometri': shape.from_shape(punkt, srid=4326), 'tid': tid})

insert = prepedata.insert().values(data)
connection.execute(insert)
connection.execute("DELETE FROM prepedata WHERE tid >= '2011-02-25 14:19:37' AND tid <= '2011-02-25 14:23:34'")
