import psycopg2
import json
from shapely.geometry import shape
connection = psycopg2.connect("dbname='gmgi300' user='postgres' host='localhost' password='GeoPandas'")

curser = connection.cursor()
curser.execute("SELECT DropTopology('løyper_topo')")
curser.execute("DROP TABLE IF EXISTS prepenettverk")
curser.execute("CREATE TABLE prepenettverk (id serial PRIMARY KEY, geometri geometry(LINESTRING, 4326))")

with open("Data/løypesammenslått.geojson") as fil:
    løypenettverk = json.load(fil)

for feature in løypenettverk.get('features'):
    geometri = shape(feature.get("geometry"))
    curser.execute("INSERT INTO prepenettverk (geometri) VALUES (ST_SetSRID(%s::geometry, 4326))", (geometri.wkb,))

curser.execute("SELECT topology.CreateTopology('løyper_topo', 4326)")
curser.execute("SELECT topology.AddTopoGeometryColumn('løyper_topo', 'public', 'prepenettverk', 'topo_geom', 'LINESTRING')")
curser.execute("UPDATE prepenettverk SET topo_geom = topology.toTopoGeom(geometri, 'løyper_topo', 1, 0)")

connection.commit()



