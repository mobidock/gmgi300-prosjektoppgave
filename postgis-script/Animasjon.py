# -*- coding: utf-8 -*-
from sqlalchemy import create_engine, MetaData, Table
from geoalchemy2 import shape
from shapely.geometry import MultiPoint, mapping
import json

engine = create_engine('postgresql://postgres:GeoPandas@localhost/gmgi300')
connection = engine.connect()
metadata = MetaData(bind=engine)

prepedata = Table('prepedata', metadata, autoload=True)

rader = connection.execute(prepedata.select().order_by(prepedata.c.tid))
koordinater = []
tidspunkter = []
features = []

for rad in rader:
    koordinater.append(shape.to_shape(rad.geometri).coords[0])
    tidspunkter.append(int(rad.tid.timestamp() * 1000))

multipunkt = MultiPoint(koordinater)
features.append({'type': 'Feature',
                 'geometry': mapping(multipunkt),
                 'properties': {'time': tidspunkter}})

with open('Data/animasjon.geojson', 'w') as geojsonfil:
    geojson = json.dumps({"type": "FeatureCollection", 'Features': features},
                         indent=4)
    geojsonfil.write(geojson)
