# -*- coding: utf-8 -*-
from sqlalchemy import create_engine, MetaData, Table, Column, DateTime
import sqlalchemy.sql as sql
from geoalchemy2 import Geometry, shape
from shapely.geometry import LineString
import datetime

engine = create_engine('postgresql://postgres:GeoPandas@localhost/gmgi300')
connection = engine.connect()
metadata = MetaData(bind=engine)

prepedata = Table('prepedata', metadata, autoload=True)
prepespor = Table('prepespor', metadata,
                  Column('geometri', Geometry('LineString', 4326), nullable=False),
                  Column('start_tid', DateTime, nullable=False),
                  Column('slutt_tid', DateTime, nullable=False))
prepespor.drop(checkfirst=True)
prepespor.create()

selection = sql.select([prepedata.c.geometri,
                        prepedata.c.tid,
                        (sql.over(sql.func.lead(prepedata.c.tid),
                                  order_by=prepedata.c.tid) -
                         prepedata.c.tid).label('tidsforskjell')]
                       ).order_by(prepedata.c.tid)

rader = connection.execute(selection)
koordinater = []
tidspunkter = []
data = []

for rad in rader:
    koordinater.append(shape.to_shape(rad.geometri).coords[0])
    tidspunkter.append(rad.tid)

    if (rad.tidsforskjell is not None and
                rad.tidsforskjell >= datetime.timedelta(hours=5)):
        linje = LineString(koordinater)
        data.append({'geometri': shape.from_shape(linje, srid=4326),
                     'start_tid': tidspunkter[0],
                     'slutt_tid': tidspunkter[-1]})
        koordinater = []
        tidspunkter = []

linje = LineString(koordinater)
data.append({'geometri': shape.from_shape(linje, srid=4326),
             'start_tid': tidspunkter[0],
             'slutt_tid': tidspunkter[-1]})

insert = prepespor.insert().values(data)
connection.execute(insert)
