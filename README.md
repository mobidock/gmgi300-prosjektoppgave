# README #


### Hva er dette? ###

Prosjektinnlevering GMGI300 - Geografiske databasesystemer for Trym og Bjørn


### Introduksjon ###
Tidligere gjeldende semester ble det dannet grupper som skulle gjennomføre
prosjektoppgaver i emnet GMGI300 på NMBU i Ås.
Tematikken for prosjektet var bearbeiding og
publisering av romlig data, fra rådata til presentasjon på web.
Løsningen skulle inneholde en PostGIS-database for lagring,
en tjeneste for å publisere, og en webløsning for å vise de romlige dataene.
Vår gruppe har tatt for seg bearbeiding og fremstilling av skiløyper i området rundt Ås.
Denne rapporten tar derfor for seg fremgangsmåte
og resultater av hvordan en kan vise både status på skiløyper,
og hvordan spor-preppingen temporært gikk for seg i Leafletbaserte web-kart.

### Prosjektinnlevering ###
Rapporten finner du i Prosjektrapport.pdf Håvard Tveite!